from ansible.errors import AnsibleParserError
from ansible.module_utils.common.text.converters import to_native
from ansible.plugins.inventory import BaseInventoryPlugin, Cacheable

import subprocess
import json


def search_knife(query):
    try:
        completed = subprocess.run(["knife", "search", query, "-a", "hostname", "-a", "fqdn", "-a", "policy_group", "-a", "ipaddress", "-a", "policy_name", "-a", "platform", "-F", "json"], capture_output=True)
    except Exception as e:
        raise AnsibleParserError("Could not talk to Chef via knife: %s" % to_native(e))

    if completed.stderr:
        raise AnsibleParserError("Completed: %s" % to_native(completed))

    return parse_nodes(completed.stdout)


def parse_nodes(knife_search_stdout):
    try:
        raw = json.loads(knife_search_stdout)
    except Exception as e:
        raise AnsibleParserError("Could not parse knife output: %s" % to_native(e))

    nodes = raw['rows']

    return nodes


class InventoryModule(BaseInventoryPlugin, Cacheable):
    NAME = 'chef'

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path, cache)

        node_list = search_knife("*:*")

        for chef_node in node_list:
            id = list(chef_node.keys())[0]
            host = chef_node[id]
            if host['hostname'] not in self.inventory.hosts:
                if host['platform'] not in self.inventory.groups:
                    self.inventory.add_group(host['platform'])
                self.inventory.add_host(host['hostname'], group=host['platform'])
                if host['policy_group'] not in self.inventory.groups:
                    self.inventory.add_group(host['policy_group'])
                self.inventory.add_host(host['hostname'], group=host['policy_group'])
                if host['policy_name'] not in self.inventory.groups:
                    self.inventory.add_group(host['policy_name'])
                self.inventory.add_host(host['hostname'], group=host['policy_name'])
                
                if 'ip-' in host['hostname']:
                    self.inventory.set_variable(host['hostname'], 'ansible_host', host['ipaddress'])
                else:
                    self.inventory.set_variable(host['hostname'], 'ansible_host', host['fqdn'])
                
                if 'windows' in host['platform']:
                    self.inventory.set_variable(host['hostname'], 'ansible_connection', 'winrm')
                    self.inventory.set_variable(host['hostname'], 'ansible_port', 5985)
                    self.inventory.set_variable(host['hostname'], 'ansible_winrm_scheme', 'http')

    def verify_file(self, path):
        valid = False
        if super(InventoryModule, self).verify_file(path):
            if path.endswith(('chef.yaml', 'chef.yml')):
                valid = True
        return valid
