# Chef inventory plugin for Ansible

This is a plugin for Ansible to allow pulling inventory from a Chef Server.
It relies on your normal knife configuration for operation.

To use, put the chef.py script in `~/.ansible/plugins/inventory/` and put
the chef.yml file whereever your Ansible code lives. Reference the chef.yml
file as an inventory file on your Ansible runs (e.g., `ansible -i chef.yml`).

Also make sure that chef is enabled as a plugin in your `~/.ansible.cfg` file.
See Ansible's documentation for details.
